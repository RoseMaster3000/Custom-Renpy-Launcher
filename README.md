# About
This is my custom version of the Renpy Front Page. It is just a modified version of [this](https://github.com/renpy/renpy/blob/master/launcher/game/front_page.rpy) file in particular. The "Launch Project" button is moved to the top right of the GUI. This is done so that the window can be dragged to the bottom left of the monitor in a way that only the "Launch Project" button is visible. **Note you can also simply run `renpy` from your project directory via CLI / setup a hotkey from your text editor.**

## Installation
* To install on Linux `source install.sh`
* On Windows just replace your front_page.rpy file manually

## Links
[Renpy](https://github.com/renpy/renpy) (v8.0.3-1)

# Author
| Shahrose Kasim |             |
|----------------|-------------|
|*[shahros3@gmail.com](mailto:shahros3@gmail.com)*|[shahrose.com](http://shahrose.com)|
|*[rosemaster3000@gmail.com](mailto:rosemaster3000@gmail.com)*|[florasoft.live](https://florasoft.live) |
|*[RoseMaster#3000](https://discordapp.com/users/122224041296789508)*|[discord.com](https://discord.com/)|